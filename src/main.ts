import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import * as dotenv from 'dotenv';
import { CustomModule } from './custom/custom.module';
dotenv.config()

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  const user = process.env.RABBITMQ_USER;
  const password = process.env.RABBITMQ_PASSWORD;
  const host = process.env.RABBITMQ_HOST;
  const queueName = process.env.RABBITMQ_QUEUE_NAME;
  const hostcloud = process.env.RABBITMQCLOUD;
  const customMicroservice = await NestFactory.createMicroservice<MicroserviceOptions>(CustomModule, {
    transport: Transport.RMQ,
    options: {
      // urls: [`amqp://${user}:${password}@${host}`],
      urls: [hostcloud],
      queue: 'queue-name',
      noAck: false,
      queueOptions: {
        durable: true,
      },
    },
  });
  customMicroservice.listen(() => Logger.debug('Listening on custom_queue'));
  app.connectMicroservice(customMicroservice);
  const PORT = process.env.PORT || 4000;
  await app.listen(PORT);
}
try {
  bootstrap();
} catch (err) {
  Logger.error('critical app error');
  Logger.debug(err);
}
