import { Body, ClassSerializerInterceptor, Controller, Get, Inject, Post, UseInterceptors } from '@nestjs/common';
import { ClientProxy, Ctx, MessagePattern, Payload, RmqContext } from '@nestjs/microservices';
import { AppService } from './app.service';
import { CreateSubscriberDto } from './create-subscriber.dto';

@Controller()
@UseInterceptors(ClassSerializerInterceptor)
export class AppController {
  constructor(
    private readonly appService: AppService,
  ) {}
  @Get()
  getHello(){
    return this.appService.getHello()
  }

  @MessagePattern({ cmd: 'add-subscriber' })
  async addSubscriber(@Payload() subscriber: CreateSubscriberDto, @Ctx() context: RmqContext) {
    const newSubscriber = await this.appService.create(subscriber);
  
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg, false, true);
    return newSubscriber;
  }
}
