import {
    Body,
  Controller,
  Get,
  Inject,
  Post,
} from '@nestjs/common';
import {
    ClientProxy
} from '@nestjs/microservices';
import { AppService } from 'src/app.service';
import { CreateSubscriberDto } from 'src/create-subscriber.dto';
import { ScrappingService } from 'src/scrapper/scrapping.service';

@Controller('custom')
export class CustomController {
  constructor(
      @Inject('APP_SERVICE') private client: ClientProxy,
      private readonly scrappingService: ScrappingService,
  ) {}

  @Get()
  async scrappingPuppeteer(){
    return await this.scrappingService.scrappingPuppeteer()
  }

  @Get('cluster')
  async scrappingPuppeteerCluster(){
    return await this.scrappingService.scrappingPuppeteerCluster()
  }

  @Post()
  async createPost(@Body() subscriber: CreateSubscriberDto) {
      const pattern = { cmd: 'add-subscriber' };
    return await this.client.send<any>(pattern , subscriber)
  }
  
}