
import { forwardRef, Module } from '@nestjs/common';
import { AppModule } from 'src/app.module';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { CustomController } from './custom.controller';
import { ScrappingModule } from 'src/scrapper/scrapping.module';

@Module({
  controllers: [CustomController],
  imports: [forwardRef(() => AppModule), ScrappingModule],
  providers:[{
      provide: 'APP_SERVICE',
      useFactory: (configService: ConfigService) => {
        const user = process.env.RABBITMQ_USER;
        const password = process.env.RABBITMQ_PASSWORD;
        const host = process.env.RABBITMQ_HOST;
        const queueName = process.env.RABBITMQ_QUEUE_NAME;
        const hostcloud = process.env.RABBITMQCLOUD;
        return ClientProxyFactory.create({
          transport: Transport.RMQ,
          options: {
            // urls: [`amqp://${user}:${password}@${host}`],
            urls: [hostcloud],
            queue: 'queue-name',
            queueOptions: {
              durable: true,
            },
          },
        })
      },
      inject: [ConfigService],
    }]
})

export class CustomModule {}