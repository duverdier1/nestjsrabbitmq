import { Body, Controller, Get, Post } from "@nestjs/common";
import { CreateProduitDto } from "./dto/create-produit.dto";
import { Produit } from "./entities/produit.entity";
import { ScrappingService } from "./scrapping.service";

@Controller('produit')
export class ProduitController {
    constructor(private scrappingService: ScrappingService){}

    @Get()
    async getProduit(): Promise<Produit[]>{
        return await this.scrappingService.getProduit()
    }
    @Post()
    async createProduitScrapper(@Body() produit: CreateProduitDto): Promise<Produit>{
       return await this.scrappingService.createProduitScrapper(produit)
    }
}