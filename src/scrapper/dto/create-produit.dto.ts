import { IsNotEmpty } from "class-validator";

export class CreateProduitDto {
    @IsNotEmpty({message: "La description est requise"})
    description: string;

    @IsNotEmpty({message: "Le prix est requis"})
    prix: string;
}