import { Injectable, Logger } from "@nestjs/common";
import { Cron, CronExpression } from "@nestjs/schedule";
import { InjectRepository } from "@nestjs/typeorm";
import * as shell from 'shelljs';
import { Repository } from "typeorm";
import { CreateProduitDto } from "./dto/create-produit.dto";
import { Produit } from "./entities/produit.entity";
import { ProduitRepository } from "./repository/produit.repository";

@Injectable()
export class ScrappingService{
    constructor(
      private produitsRepository: ProduitRepository,
    ){}
    private readonly logger = new Logger(ScrappingService.name);

    // @Cron(CronExpression.EVERY_10_SECONDS)
    async scrappingPuppeteer(){
        const datas = await shell.exec(`npx ts-node ./src/scrapper/scraping/scrapping-puppeteer.ts`);
        console.log('datas: ', datas);
        // datas.map((data) => {
        //     this.createProduitScrapper(data)
        // })
        return { message: "Scrapping terminé", datas }
    }

    async scrappingPuppeteerCluster(){
        const data = await shell.exec(`npx ts-node ./src/scrapper/scraping/scraping-cluster.ts`);
        return { message: "Scrapping terminé", data }
    }

    async createProduitScrapper(produit: CreateProduitDto): Promise<Produit>{
        let produitCreated = new Produit()
        Object.assign(produitCreated, produit)
        try {
            return await this.produitsRepository.save(produit);
        } catch (error) {
            console.log('error: ', error);
        }
    }

    async getProduit(): Promise<Produit[]>{
        return await this.produitsRepository.find()
    }

}