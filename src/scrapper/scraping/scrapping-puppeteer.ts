import * as puppeteer from 'puppeteer';
const url = 'https://afrikdiscount.com/fr/';

const getDescription = async (page) => {
  await page.waitForTimeout(2000);
  const copie = [];
  const texts = await page.$$eval('span.price.product-price',
  divs => divs.map(({ innerText }) => innerText));
  await page.waitForTimeout(2000);
  const texts1 = await page.$$eval('a.product-name',
  divs => divs.map(({ innerText }) => innerText));
  texts.map((text, index) => {
    copie.push({
      prix: text,
      description: texts1[index]
    });
  });
  await page.waitForTimeout(2000);
  return copie;
}
const getPagginate = async (page) => {
  const texts = await page.$$eval('#pagination > ul > li > a > span',
  divs => divs.map(({ innerHTML }) => innerHTML));
  return texts;
}
(async () => {
  let results = [];
  try {
    if (!url) {
      throw "Please provide URL as a first argument";
    }
    const browser = await puppeteer.launch({
      args: ['--no-sandbox'],
    });
    const page = await browser.newPage();
    await page.setViewport({
      width: 1680,
      height: 920
    });
    await page.setDefaultNavigationTimeout(0); 
    await page.goto(url, { waitUntil:"networkidle2" });
    await page.waitForTimeout(500);

    let data = await page.evaluate(() => {
      return document.querySelector('a[title="ELECTRONIQUE"]');
    })
    if(data){
      await page.click('a[title="ELECTRONIQUE"]');
    }
    await page.waitForTimeout(5000);
    const data1 = await getPagginate(page);
    let number = 0;
    for(const dr of data1){
      number = parseInt(dr);
    }
    await page.waitForTimeout(1000);
    
    for (let index = 1; index < number; index++) {
      await page.waitForTimeout(1000);
      const produitDescription = await getDescription(page);
      results = results.concat(produitDescription)
      const pagination = await page.$("#pagination_next > a");
      await pagination.click();
    }
    await page.waitForTimeout(5000);
    await browser.close();
  } catch (error) {
    console.log("error ",error);
  }
  return results
})()