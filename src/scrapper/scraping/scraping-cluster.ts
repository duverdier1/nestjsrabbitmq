import * as puppeteer from 'puppeteer';
const { Cluster } = require('puppeteer-cluster');
const getDescription = async (page) => {
  await page.waitForTimeout(2000);
  const copie = [];
  const texts = await page.$$eval('span.price.product-price',
  divs => divs.map(({ innerText }) => innerText));
  await page.waitForTimeout(2000);
  const texts1 = await page.$$eval('a.product-name',
  divs => divs.map(({ innerText }) => innerText));
  texts.map((text, index) => {
    copie.push({
      prix: text,
      description: texts1[index]
    });
  });
  await page.waitForTimeout(2000);
  return copie;
}
const getPagginate = async (page) => {
  const texts = await page.$$eval('#pagination > ul > li > a > span',
  divs => divs.map(({ innerHTML }) => innerHTML));
  return texts;
}
(async () => {
    let results = [];
    try {
        const cluster = await Cluster.launch({
            concurrency: Cluster.CONCURRENCY_PAGE,
            maxConcurrency: 4,
            monitor: true,
            timeout: 500000,
            puppeteerOptions: {
                args: ['--no-sandbox'],
            },
            skipDuplicateUrls: true,
        });
       
        cluster.on('taskerror', (err, data, willRetry) => {
            if (willRetry) console.warn(`Erreur de scraping, réessai prévu : `);
            else console.error(`Erreur de scraping :`);
            console.log("err", err);
        });
        
        await cluster.task(async ({ page, data: url }) => {
            await page.setViewport({
                width: 1279,
                height: 920
            });
            await page.setDefaultNavigationTimeout(0);
            await page.goto(url);
            let data = await page.evaluate(() => {
                return document.querySelector('a[title="ELECTRONIQUE"]');
            })
            if(data){
                console.log("ok");
                await page.click('a[title="ELECTRONIQUE"]');
            }
            await page.waitForTimeout(5000);
            const data1 = await getPagginate(page);
            await page.waitForTimeout(1500);
            let number = 0;
            for(const dr of data1){
                number = parseInt(dr);
            }
            await page.waitForTimeout(1000);
            for (let index = 1; index < number; index++) {
                await page.waitForTimeout(1000);
                const produitDescription = await getDescription(page);
                results = results.concat(produitDescription)
                const pagination = await page.$("#pagination_next > a");
                await pagination.click();
            }
        });
        
        cluster.queue('https://afrikdiscount.com/fr');
        
        await cluster.idle();
        await cluster.close();
    } catch (error) {
        console.log("error ",error);
    }
    return results;
})();