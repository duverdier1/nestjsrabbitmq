
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProduitRepository } from './repository/produit.repository';
import { ProduitController } from './scraping.controller';
import { ScrappingService } from './scrapping.service';

@Module({
  imports: [TypeOrmModule.forFeature([ProduitRepository])],
  controllers: [ProduitController],
  providers:[ScrappingService],
  exports: [ScrappingService]
})

export class ScrappingModule {}