import { Injectable } from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";
import { CreateProduitDto } from "../dto/create-produit.dto";
import { Produit } from "../entities/produit.entity";

@Injectable()
@EntityRepository(Produit)
export class ProduitRepository extends Repository<Produit> {
    public constructor(){
        super();
    }
}