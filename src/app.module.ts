import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomModule } from './custom/custom.module';
import { Produit } from './scrapper/entities/produit.entity';
import { ScrappingModule } from './scrapper/scrapping.module';

@Module({
  imports: [
    // TypeOrmModule.forRoot({
    //   type: 'postgres',
    //   host: 'localhost',
    //   port: 5432,
    //   username: 'postgres',
    //   password: 'root',
    //   database: 'rabbit',
    //   entities: [Produit],
    //   synchronize: true,
    //   keepConnectionAlive: true,
    // }),
    ConfigModule.forRoot({
            ignoreEnvFile: true,
            isGlobal: true,
        }),
      ScheduleModule.forRoot(),
      ConfigModule,
      CustomModule,
      ScrappingModule,
      TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'ec2-3-89-0-52.compute-1.amazonaws.com',
      port: 5432,
      username: 'gfqgchgqxbasso',
      password: '90c6879085a11d0d8f94b0260bffafc05a7e219b37f2037c5649a22f02c90508',
      database: 'd8fuoj73fubr72',
      entities: [Produit],
      synchronize: false,
      keepConnectionAlive: true,
      autoLoadEntities: true,
      ssl : { rejectUnauthorized: false }
      }),
      ],
  controllers: [AppController],
  providers: [AppService],
  exports:[AppService]
})
export class AppModule {}
