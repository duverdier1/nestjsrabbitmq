import { Injectable } from '@nestjs/common';
import { Ctx, MessagePattern, Payload, RmqContext } from '@nestjs/microservices';
import { Subscriber } from './cat.interface';
import { CreateSubscriberDto } from './create-subscriber.dto';

@Injectable()
export class AppService {
  private readonly subscribers: Subscriber[] = [];
  getHello() {
    return this.subscribers;
  }

  create(subscriber: Subscriber) {
    return this.subscribers.push(subscriber);
  }
}
